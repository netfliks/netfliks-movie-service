package com.netfliks.movieservice.controllers;

import com.netfliks.movieservice.domain.Movie;
import com.netfliks.movieservice.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MovieController {

    @Autowired
    MovieRepository repo;

    @PostMapping("/add")
    public String addMovie(){
        repo.save(new Movie(0L, "Example movie", "https://via.placeholder.com/200x320", "https://v.redd.it/9l3yetwjyh551/DASH_480?source=fallback"));
        return "Added movie geslaagd";
    }

    @GetMapping()
    public void getMovie(@RequestBody String movieName) {
        // TODO
    }

    @GetMapping("/all")
    public List<Movie> getAllMovies(){
        return repo.findAll();
    }

    @PutMapping
    public void updateMovie() {
        // TODO
    }

    @DeleteMapping
    public void deleteMovie() {
        // TODO
    }

    @GetMapping("/ping")
    public String test(){
        return "Movie returned ping";
    }
}
